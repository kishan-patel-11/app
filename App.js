import React from 'react';
import {SafeAreaView, View} from 'react-native';

const App = () => {
  return (
    <SafeAreaView style={{flex: 1, backgroundColor: 'white'}}>
      <View style={{flex: 1, backgroundColor: 'red'}} />
    </SafeAreaView>
  );
};

export default App;
